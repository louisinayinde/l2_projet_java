package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.List;

public class PokerBoard implements Board {

    List<Card> cards;
    List<Coin> coins;

	public PokerBoard() {
        this.cards = new ArrayList<>();
        this.coins = new ArrayList<>();
    }
	
    @Override
    public void clear() {
        this.cards.clear();
        this.coins.clear();
    }

    @Override
    public void addComponent(Component component) {
        if(component instanceof Card){
            this.cards.add((Card)component);
        }
        else if(component instanceof Coin){
            this.coins.add((Coin)component);
        }
    }

    @Override
    public List<Component> getComponents() {
        List<Component> temp =  new ArrayList<>();
        temp.addAll(this.coins);
        temp.addAll(this.cards);
        return  temp;
    }
    @Override
    public List<Component> getSpecificComponents(Class classType) {
        if(classType.isInstance(this.cards)){
            return new ArrayList<>(this.cards);
        }
        else if(classType.isInstance(this.coins)){
            return new ArrayList<>(this.coins);
        }
        return null;
    }

    @Override
    public void displayState() {

        System.out.println("--------------");
        for (Card c: this.cards){
            System.out.println("Name : " + c.getName());
        }
        for (Coin co: this.coins){
            System.out.println("Name : " + co.getName() + " value : "+co.getValue());
        }

    }
}
