package ulco.cardGame.common.games.boards;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Board;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardBoard implements Board {

    List<Card> cards;
	
	public CardBoard() {
        this.cards = new ArrayList<>();
    }
	
    /*
     * Supprime les cartes
     */
    @Override
    public void clear() {
        this.cards.clear();
    }

    /*
     * ajoute le component d'une carte
     */
    @Override
    public void addComponent(Component component) {
        cards.add((Card) component);
    }

    /*
     * retourne le component d'une carte
     */
    @Override
    public List<Component> getComponents() {
        return new ArrayList<>(this.cards);
    }

    /*
     * retourne le component de toute les cartes
     */
    @Override
    public List<Component> getSpecificComponents(Class classType) {
        if(classType.isInstance(this.cards)){
            return new ArrayList<>(this.cards);
        }
        return null;
    }

    /*
     * affiche les cartes
     */
    @Override
    public void displayState() {
        for (Card c: this.cards){
            System.out.println("Name of card : " + c);
        }
    }
}
