package ulco.cardGame.common.players;

import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Coin;
import ulco.cardGame.common.games.components.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class PokerPlayer extends BoardPlayer{

    List<Card> cards;
    List<Coin> coins;

    public PokerPlayer(String name) {
        super(name);
		this.cards = new ArrayList<>();
        this.coins = new ArrayList<>();
    }

    @Override
    public Integer getScore() {
        return this.score;
    }

    @Override
    public Component play()  {

        Coin temp_coins;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Donner une couleur de jeton");
        String msg = scanner.nextLine();
        for(Coin c : this.coins){
            if(c.getName() == msg){
                temp_coins = c;
                this.coins.remove(temp_coins);
                return temp_coins;
            }
        }
        return null;
    }

    @Override
    public void addComponent(Component component) {
        if(component instanceof Card){
            this.cards.add((Card)component);
        }
        else if(component instanceof Coin){
            this.coins.add((Coin)component);
            this.score += component.getValue();
        }

    }

    @Override
    public void removeComponent(Component component) {

        if(component instanceof Card){
            this.cards.remove((Card)component);
        }
        else if(component instanceof Coin){
            this.coins.remove((Coin)component);
            this.score -= component.getValue();
        }
    }

    @Override
    public List<Component> getComponents() {
        List<Component> temp = new ArrayList<>();
        temp.addAll(cards);
        temp.addAll(coins);

        return temp;
    }

    public List<Component> getSpecificComponents(Class classType) {
        if(classType.isInstance(this.cards)){
            return new ArrayList<>(this.cards);
        }
        else if(classType.isInstance(this.coins)){
            return new ArrayList<>(this.coins);
        }
        return null;
    }

    @Override
    public void shuffleHand() {
        // prepare to shuffle hand
        Collections.shuffle(cards);
    }

    @Override
    public void clearHand() {

        // by default clear player hand
        // unlink each card
        for (Card card : this.cards) {
            card.setPlayer(null);
        }
        this.cards = new ArrayList<>();

        for (Coin coin : this.coins) {
            coin.setPlayer(null);
        }
        this.coins = new ArrayList<>();

    }

    public void displayHand(){
        int sum=0;
        System.out.println("--------------");
        for (Card c: this.cards){
            System.out.println("Name : " + c.getName());
        }
        for (Coin co: this.coins){
            System.out.println("Name : " + co.getName() + " value : "+co.getValue());
            sum += co.getValue();
        }
        System.out.println("your coin sum is : " + sum);
    }



    public String toString(){
        return "name='" + name + "' ; score=" + score;
    }
}
