package ulco.cardGame.common.interfaces;

import ulco.cardGame.common.games.components.Component;

import java.util.List;

public interface Board {
    /**
     *
     */
    void clear();

    /**
     * Add Component to the current Game
     */
    void addComponent(Component component);

    /**
     *
     */
    List<Component> getComponents();

    /**
     *
     */
    List<Component> getSpecificComponents(Class classType);

    /**
     * Display current Game state
     * Such as user state (name and score)
     */
    void displayState();

}

